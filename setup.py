#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Install script for package"""

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    name='gw_compare',
    description='Utilities for waveform reconstruction comparisons',
    version='0.1',
    author='James Alexander Clark, Sudarshan Ghonge',
    author_email='james.clark@ligo.org',
    url='https://git.ligo.org/james-clark/gw-compare',
    scripts=['scripts/gwcomp_driver.py', 'scripts/gwcomp_reclal.py',
             'scripts/gwcomp_residuals.py', 'scripts/gwcomp_testgr.py',
             'scripts/gwcomp_qscans.py', 'scripts/gwcomp_pos2siminspiral.py',
             'scripts/gwcomp_map2siminspiral.py', 'scripts/gwcomp_injtimes.py',
             'scripts/gwcomp_combinemedians.py'],
    packages=['gw_compare']
)
