#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Copyright (C) 2016-2017 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
frame2ascii.py

Example script to read a subset of data directly from a frame and write that
data to ascii

USAGE: frame2ascii.py --input-frame <frame.gwf> --channel <channel-name> 
	--gps-start <gps time> --duration <duration>

        Defaults:  
            output-frame: determined from original frame and requested time
            channel: L1:GDS-CALIB_STRAIN (see e.g., FrChannels <frame.gwf>)

"""

import sys, string
import numpy as np
from optparse import OptionParser
import pycbc.frame
import pycbc.filter

def parser():
    """ 
    Parser for input (command line and ini file)
    """

    # --- cmd line
    parser = OptionParser()
    parser.add_option("-i", "--input-frame", type=str)
    parser.add_option("-o", "--output-name", type=str)
    parser.add_option("-c", "--channel", type=str, default="L1:GDS-CALIB_STRAIN")
    parser.add_option("-s", "--gps-start", type=int)
    parser.add_option("-d", "--duration", type=int)

    (opts,args) = parser.parse_args()

    if opts.output_name is None:
        # Construct the new frame automatically
        frame_parts = opts.input_frame.split('/')[-1].split('-')
        frame_parts[2] = str(opts.gps_start)
        frame_parts[3] = str(opts.duration)+'.asc'
        opts.output_name = string.join(frame_parts,'-')
    
    return opts, args


def main():

    # Parse input:
    opts, args = parser()

    print >> sys.stdout, "Reading {0} from {1}".format(
            opts.channel, opts.input_frame)

    # Read duration (s) from frame
    frame_data = pycbc.frame.read_frame(location=opts.input_frame,
            channels=opts.channel, start_time=opts.gps_start,
            duration=opts.duration)

    print >> sys.stdout, "...Data read succesfully"

#   # XXX: Operate on the frame_data TimeSeries object (data is in the array
#   # frame_data.data)
    import matplotlib
    matplotlib.use("Agg")
    from matplotlib import pyplot as pl
    f, ax = pl.subplots()
    ax.plot(frame_data.sample_times-frame_data.sample_times[0], frame_data.data)
    ax.set_xlim(15.5, 16.5)
    f.savefig('unfiltered_%s'%opts.output_name.replace('asc', 'png'))

    frame_data_highpass = pycbc.filter.highpass(frame_data, 10.0)

    f, ax = pl.subplots()
    ax.plot(frame_data_highpass.sample_times-frame_data.sample_times[0], frame_data_highpass.data)
    ax.set_xlim(15.9, 16.1)
    f.savefig('highpassed_%s'%opts.output_name.replace('asc', 'png'))


    print >> sys.stdout, "Writing subset of data to {0} in {1}".format(
            opts.channel, opts.output_name)
    #pycbc.frame.write_frame(opts.output_name, opts.channel, frame_data)
    np.savetxt(opts.output_name, np.transpose([frame_data.sample_times,
        frame_data.data, frame_data_highpass.data]))



    print >> sys.stdout, "FINISHED"



if __name__ == "__main__":

    main()

